# Starting a new Project

## Assumption

* Admin privileges will be required to some of the services depicted in this guide. They will be marked with a (A)
* An Apple Developer account has already been setup with the code signing identities and provisioning profiles

If you do not have access, please pair with a team lead.

## Bitbucket Project Setup (A)

1. First, you will need to create the new repo:
    * Select `outware` as the *Owner*
    * Select *Outware's Project* as the Project
    * Follow the naming convention: *<CodeName - Platform>*
    * Make sure the repository is **private**
    * Set a platform language
1. Then, a new Bitbucket Team should be created (e.g.: Team-Project-Platform).
    * Make sure the group does not get access to any other repositories
1. And finally add the new team to the repo with *Write* access


## Repository Setup

### OMProject Forking

1. Clone OMProject
        git clone git@bitbucket.org:outware/omproject-ios-swift.git <project name>
1. Set the new remote

        git remote set-url origin <new repo url>

        git push --set-upstream

        git checkout -b master

        git pushup --set-upstream

        git submodule update --init


### OMProject Renaming

1. Checkout on a new branch
        git checkout -b feature/project-rename
1. Rename the Xcode project
    1. Select the project in the project navigator
    1. Press enter, and rename the project to the project name
1. Rename the scheme
1. Rename the `PROJECT_NAME` in the *build settings* of the **project**
1. Rename the `import` statements to the new project name (e.g.: `@testable import OMProject` -> `@testable import ProjectName`)
1. Close Xcode
1. Rename `OMProject.xcworkspace` file to `ProjectName.xcworkspace`
1. Update the location of the project in the `ProjectName.xcworkspace/contents.xcworkspacedata` file (e.g.: `location = "group:ProjectName.xcodeproj"`)
1. Open the workspace and run the tests
1. Review the changes, commit them and open a Pull Request

## Fastlane Setup

1. Checkout on a new branch
        git checkout -b feature/fastlane-setup
1. Install required ruby dependencies
        bundle install --local --path vendor/bundle
1. Edit `fastlane/.env` to customise *Fastlane* to your project.

### Hockey Setup (A)

The following steps will only depict a high level guide to creating new apps on HockeyApp.

1. Create 2 new apps: *ProjectName - OMQA* and *ProjectName - DropClient*
1. Add the developers, project manager and client to the apps
1. Create the API tokens
1. Update the API tokens under `fastlane/.env`
